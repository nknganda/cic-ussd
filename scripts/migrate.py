#!/usr/bin/python

import os
import logging

import alembic
from alembic.config import Config as AlembicConfig

import config

logg = logging.getLogger(__name__)

rootdir = os.path.dirname(os.path.dirname(__file__))
migrationsdir = os.path.join(rootdir, 'db', 'migrations')
logg.info('using migrationsdir {}'.format(migrationsdir))

c = config.config_from_environment()

dsn = 'postgresql+psycopg2://{}:{}@{}:{}/{}'.format(
    c.get('DATABASE_USER'),
    c.get('DATABASE_PASSWORD'),
    c.get('DATABASE_HOST'),
    c.get('DATABASE_PORT'),
    c.get('DATABASE_NAME'),    
    )

ac = AlembicConfig(os.path.join(migrationsdir, 'alembic.ini'))
ac.set_main_option('script_location', migrationsdir)
ac.set_main_option('sqlalchemy.url', dsn)

alembic.command.upgrade(ac, 'head')
