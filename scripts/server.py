"""Functions defining WSGI interaction with external http requests
Defines an application function essential for the uWSGI python loader to run th python application code.
"""
# standard imports
import os
import logging
import json

# third-party imports
import i18n
from confini import Config
from sqlalchemy import create_engine

# local imports
from session.ussd_session import UssdSession
from menu.ussd_menu import UssdMenu
from models.base import SessionBase
from util import PasswordEncoder
from tasks.ussd import persist_session_to_db

logg = logging.getLogger(__name__)

# parse config
config = Config(f'{os.environ.get("CONFIG_DIR")}/{os.environ.get("DEPLOYMENT_NAME")}/')
config.process()

session_user = {}

# set up translations
i18n.load_path.append(config.get('PLATFORM_LOCALE_PATH'))
i18n.set('fallback', config.get('PLATFORM_LOCALE_FALLBACK'))

# set Fernet key
PasswordEncoder.set_key(config.get('PLATFORM_PASSWORD_PEPPER'))

# set up db
db_engine = create_engine('postgresql+psycopg2://{}:{}@{}:{}/{}'.format(
    config.get('DATABASE_USER'),
    config.get('DATABASE_PASSWORD'),
    config.get('DATABASE_HOST'),
    config.get('DATABASE_PORT'),
    config.get('DATABASE_NAME'),
))
SessionBase.set_engine(db_engine)


def check_ip(env):
    """
    Check whether request origin IP is whitelisted
    :param env: Object containing server and request information
    :type env: dict
    :return: Request IP validity
    :rtype: boolean
    """
    return env.get('REMOTE_ADDR') == config.get('API_ALLOWED_IP')


def check_request_method(env):
    """
    Checks whether request method is POST
    :param env: Object containing server and request information
    :type env: dict
    :return: Request method validity
    :rtype: boolean
    """
    return env.get('REQUEST_METHOD').upper() == 'POST'


def check_request_content_length(env):
    """
    Checks whether the request's content is less than or equal to the system's set maximum content length
    :param env: Object containing server and request information
    :type env: dict
    :return: Content length validity
    :rtype: boolean
    """
    return env.get('CONTENT_LENGTH') is not None and int(env.get('CONTENT_LENGTH')) <= int(config.get('PLATFORM_MAX_BODY_LENGTH'))


def check_service_code(code):
    """
    Checks whether provided code matches expected service code
    :param code: Service code passed over request
    :type code: str
    :return: Service code validity
    :rtype: boolean
    """
    logg.debug(f'RECEIVED CODE: {code}')
    logg.debug(f'API SERVICE CODE: {config.get("API_SERVICE_CODE")}')
    logg.debug(f'COMPARISON: {code == config.get("API_SERVICE_CODE")}')
    return code == config.get('API_SERVICE_CODE')


def check_phone_number(number):
    """
    Checks whether phone number is present
    :param number: A valid phone number
    :type number: str
    :return: Phone number presence
    :rtype: boolean
    """
    return number is not None


def check_known_user(phone):
    """
    This method attempts to ascertain whether the user already exists and is known to the system.
    It sends a get request to the platform application and attempts to retrieve the user's data which it persists in
    memory.
    :param phone: A valid phone number
    :type phone: str
    :return: Is known phone number
    :rtype: boolean
    """
    return UssdSession.have_session_for_phone(phone)


def check_session_id(session_id):
    """
    Checks whether session id is present
    :param session_id: Session id value provided by AT
    :type session_id: str
    :return: Session id presence
    :rtype: boolean
    """
    return session_id is not None


def check_input(user_input):
    return user_input.find('*') > -1


def get_latest_input(user_input):
    return user_input.split('*')[-1]


def create_ussd_session(session_id, phone, service_code, user_input, current_menu):
    """
    Creates a new ussd session
    :param session_id: Session id value provided by AT
    :type session_id: str
    :param phone: A valid phone number
    :type phone: str
    :param service_code: service code passed over request
    :type service_code AT service code
    :param user_input: Input from the request
    :type user_input: str
    :param current_menu: Menu that is currently being displayed on the ussd session
    :type current_menu: UssdMenu
    :return: ussd session object
    :rtype: Session
    """
    session = UssdSession(
        session_id=session_id,
        msisdn=phone,
        user_input=user_input,
        state=current_menu.name,
        service_code=service_code
    )
    session.ussd_menu = current_menu
    UssdSession.session.add(session)
    return session


def update_ussd_session(session, user_input, current_menu):
    """
    Updates a ussd session
    :param session: Session id value provided by AT
    :type session: UssdSession
    :param user_input: Input from the request
    :type user_input: str
    :param current_menu: Menu that is currently being displayed on the ussd session
    :type current_menu: UssdMenu
    :return: ussd session object
    :rtype: UssdSession
    """
    session.user_input = user_input
    session.ussd_menu = current_menu
    session.state = current_menu.name

    return session


def create_or_update_session(session_id, phone, service_code, user_input, current_menu):
    """
    Handles the creation or updating of session as necessary.
    :param session_id: Session id value provided by AT
    :type session_id: str
    :param phone: A valid phone number
    :type phone: str
    :param service_code: service code passed over request
    :type service_code: AT service code
    :param user_input: input from the request
    :type user_input: str
    :param current_menu: menu that is currently being displayed on the ussd session
    :type current_menu: UssdMenu
    :return: ussd session object
    :rtype: UssdSession
    """
    existing_session = UssdSession.session.query(UssdSession).filter_by(session_id=session_id).first()
    if existing_session:
        session = update_ussd_session(existing_session, user_input, current_menu)
    else:
        session = create_ussd_session(session_id, phone, service_code, user_input, current_menu)
    return session


def create_invalid_exit_response():
    """
    Defines a ussd menu string corresponding to the 'exit_invalid_request' key
    :return: 'exit_invalid_request' value
    :rtype: str
    """
    response = 'END '
    response += i18n.t(key='ussd.kenya.exit_invalid_request', locale='sw')
    response += '\n'
    response += i18n.t('ussd.kenya.exit_invalid_request', locale='en')
    return response


def application(env, start_response):
    """
    Loads python code for application to be accessible over web server
    :param env: Object containing server and request information
    :type env: dict
    :param start_response: Callable to define responses.
    :type start_response: any
    """
    errors_headers = [
        ('Content-Type', 'text/plain'),
        ('Content-Length', '0'),
    ]

    if not check_ip(env):
        start_response('403 Sneaky, sneaky', errors_headers)
        return []

    if not check_request_method(env):
        start_response('405 Play by the rules', errors_headers)
        return []

    if not check_request_content_length(env):
        start_response('400 Size matters', errors_headers)
        return []

    headers = [('Content-Type', 'text/plain')]
    response = ''

    # parse input, check request sanity
    post_data = json.load(env.get('wsgi.input'))

    service_code = post_data.get('serviceCode')
    phone_number = post_data.get('phoneNumber')
    session_id = post_data.get('sessionId')
    user_input = post_data.get('text')

    if not check_service_code(service_code):
        response = 'END '
        response += i18n.t(key='ussd.kenya.invalid_service_code',
                           valid_service_code=config.get('API_SERVICE_CODE'), locale='sw')
        response += '\n'
        response += i18n.t('ussd.kenya.invalid_service_code',
                           valid_service_code=config.get('API_SERVICE_CODE'), locale='en')
    # TODO[Philip]: Propose that we display top most error exits in both languages like invalid service code
    elif phone_number:
        latest_input = get_latest_input(user_input)
        if not check_known_user(phone_number):
            current_menu = UssdMenu.find_by_name('initial_language_selection')
            ussd_session = create_or_update_session(
                session_id=session_id,
                phone=phone_number,
                service_code=service_code,
                user_input=latest_input,
                current_menu=current_menu,
                )
            response = 'would have processed...'
            # KenyaUssdProcessor.custom_display_text(current_menu, ussd_session)
            persist_session_to_db.delay(session_id)
        else:
            current_menu = UssdMenu.find_by_name('exit_invalid_request')
            response = current_menu
    else:
        current_menu = UssdMenu.find_by_name('exit_invalid_request')
        response = current_menu
    # format output and we're done
    response_bytes = response.encode('utf-8')
    content_length = len(response_bytes)
    headers.append(('Content-Length', str(content_length)))
    start_response('200 OK', headers)
    return [response_bytes]
