import os
import logging

from sqlalchemy import create_engine

import config
from models.ussd import UssdMenu
from models.base import SessionBase

logg = logging.getLogger()

scriptdir = os.path.dirname(__file__)
rootdir = os.path.dirname(os.path.dirname(scriptdir))
configdir = os.path.join(rootdir, 'config', 'test')

conf = config.Config(configdir)
conf.process()
logg.info('config loaded:\n{}'.format(conf))

db_engine = create_engine('postgresql+psycopg2://{}:{}@{}:{}/{}'.format(
    conf.get('DATABASE_USER'),
    conf.get('DATABASE_PASSWORD'),
    conf.get('DATABASE_HOST'),
    conf.get('DATABASE_PORT'),
    conf.get('DATABASE_NAME'),    
    ))

SessionBase.set_engine(db_engine)
u = UssdMenu.find_by_name('exit_account_creation_prompt')
