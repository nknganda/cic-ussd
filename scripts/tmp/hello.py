import os
import logging

import celery

import config
from adapters.user import UserAdapter

logg = logging.getLogger()


celery_app = celery.Celery('ussd',
            broker = 'redis://localhost:6379',
            backend = 'redis://localhost:6379',
            )
celery_app.set_current()

if __name__ == '__main__':
    scriptpath = os.path.dirname(os.path.realpath(__file__))
    rootpath = os.path.dirname(scriptpath)
    confpath = os.path.join(rootpath, 'config', 'test')
    current_config = config.Config(confpath)
    current_config.process()
    u = UserAdapter(current_config, celery_app, '+25412121212')
    u.register_without_key()
