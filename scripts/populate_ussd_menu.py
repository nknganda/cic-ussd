"""
This script is responsible for populating the USSD menu JSON DB using the data provided in the USSD menu seed text.
"""
# local imports
from util import seed_file
from menu.ussd_menu import UssdMenu


def populate_ussd_menu():
    ussd_menu = []
    menu_index = {'\\N': None}

    with open(seed_file, 'r') as file:
        for line in file.readlines():
            line_list = line.split('\t')
            ussd_menu.append(line_list)
            menu_index[line_list[0]] = line_list[1]

    for menu in ussd_menu:
        menu[3] = menu_index[menu[3]]
        UssdMenu(menu[1], menu[2], menu[3])
        print('Menu Added!')
