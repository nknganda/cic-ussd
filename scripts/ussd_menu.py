# standard imports
import sys
import os
import logging

# third-party imports
from sqlalchemy import create_engine

# local imports
import config
from models.ussd import UssdMenu
from models.base import SessionBase

logg = logging.getLogger()


scriptdir = os.path.dirname(__file__)
rootdir = os.path.dirname(scriptdir)
configdir = os.path.join(rootdir, 'config', 'test')

conf = config.Config(configdir)
conf.process()
logg.info('config loaded:\n{}'.format(conf))

db_engine = create_engine('postgresql+psycopg2://{}:{}@{}:{}/{}'.format(
    conf.get('DATABASE_USER'),
    conf.get('DATABASE_PASSWORD'),
    conf.get('DATABASE_HOST'),
    conf.get('DATABASE_PORT'),
    conf.get('DATABASE_NAME'),    
    ))

session = SessionBase.create_session(db_engine)

f = open(sys.argv[1], "r")
for l in f.readlines():
    cols = l.split("\t")
    u = UssdMenu()
    u.id = int(cols[0])
    u.name = cols[1]
    u.description = cols[2]
    u.parent_id = cols[3]
    u.display_key = cols[4]
    if not u.parent_id.isnumeric():
        u.parent_id = None

        
    session.add(u)
    session.commit()

session.flush()
f.close()
