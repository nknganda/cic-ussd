# standard imports
import logging

# third party imports
import celery

# local imports
from tasks.adapter import TasksAdapter

logg = logging.getLogger()


# TODO: uncomment when found out how to work queues with pytest-celery
#q = 'ussdee'


class UserAdapter(TasksAdapter):
    
    def __init__(self, config, app, phone):
        logg.debug('app setting {}'.format(app))
        self.app = app
        self.phone = phone
        self.config = config

    def register_without_key(self):
        sig_create_wallet = self.app.signature('eth_manager.celery_tasks.create_new_blockchain_wallet')
        sig_register_platform_user = self.app.signature(
                'tasks.user.register_platform_user',
                args=[self.phone],
                kwargs={
                    'host': self.config.get('PLATFORM_HOST'),
                    'port': self.config.get('PLATFORM_PORT'),
                    'endpoint': '/',
                    }
                )
        sig_foo = self.app.signature('tasks.foo.log_it_plz')
        sig_create_wallet.link(sig_register_platform_user)
        sig_register_platform_user.link(sig_foo)
        r = sig_create_wallet.apply_async()
        return r
