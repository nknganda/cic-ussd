# standard imports
import urllib
import json
import logging

# local imports
from transport.interface import Transport

logg = logging.getLogger()

class HTTPJSONTransport(Transport):

    def get(self, locator, **kwargs):
        req = urllib.request.Request(locator)
        for h in kwargs['headers']:
            logg.debug('adding header {} {}'.format(h, kwargs['headers'][h]))
            req.add_header(h, kwargs['headers'][h])

        data_json = json.dumps(kwargs.get('data', {}))
        data_bytes = data_json.encode('utf-8')
        req.add_header('Content-Length', len(data_bytes))
        req.data = data_bytes
        logg.debug(req)
        res = urllib.request.urlopen(req)
        return res.code

