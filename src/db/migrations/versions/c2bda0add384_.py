"""refactor ussd session and delete ussd menu

Revision ID: c2bda0add384
Revises: b5ab9371c0b8
Create Date: 2020-10-09 17:38:03.248953

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql


# revision identifiers, used by Alembic.
revision = 'c2bda0add384'
down_revision = '2a329190a9af'
branch_labels = None
depends_on = None


def upgrade():
    op.drop_index('ix_ussd_session_session_id', table_name='ussd_session')
    op.drop_table('ussd_session')
    op.drop_index('ix_ussd_menu_name', table_name='ussd_menu')
    op.drop_table('ussd_menu')
    op.create_table('ussd_session',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('created', sa.DateTime(), nullable=True),
    sa.Column('updated', sa.DateTime(), nullable=True),
    sa.Column('session_id', sa.String(), nullable=False),
    sa.Column('service_code', sa.String(), nullable=False),
    sa.Column('msisdn', sa.String(), nullable=False),
    sa.Column('user_input', sa.String(), nullable=True),
    sa.Column('state', sa.String(), nullable=False),
    sa.Column('session_data', postgresql.JSON(astext_type=sa.Text()), nullable=True),
    sa.Column('ussd_menu', postgresql.JSON(astext_type=sa.Text()), nullable=False),
    sa.Column('version', sa.Integer(), nullable=False),
    sa.Column('user_id', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['user_id'], ['user.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_ussd_session_session_id'), 'ussd_session', ['session_id'], unique=True)


def downgrade():
    op.drop_index('ix_ussd_session_session_id', table_name='ussd_session')
    op.drop_table('ussd_session')
    op.create_table('ussd_menu',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('created', sa.DateTime(), nullable=True),
    sa.Column('updated', sa.DateTime(), nullable=True),
    sa.Column('name', sa.String(), nullable=False),
    sa.Column('description', sa.String(), nullable=True),
    sa.Column('parent_id', sa.Integer(), nullable=True),
    sa.Column('display_key', sa.String(), nullable=False),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_ussd_menu_name'), 'ussd_menu', ['name'], unique=True)
    op.create_table('ussd_session',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('created', sa.DateTime(), nullable=True),
    sa.Column('updated', sa.DateTime(), nullable=True),
    sa.Column('session_id', sa.String(), nullable=False),
    sa.Column('service_code', sa.String(), nullable=False),
    sa.Column('msisdn', sa.String(), nullable=False),
    sa.Column('user_input', sa.String(), nullable=True),
    sa.Column('state', sa.String(), nullable=False),
    sa.Column('session_data', postgresql.JSON(astext_type=sa.Text()), nullable=True),
    sa.Column('ussd_menu_id', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['ussd_menu_id'], ['ussd_menu.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_ussd_session_session_id'), 'ussd_session', ['session_id'], unique=True)
