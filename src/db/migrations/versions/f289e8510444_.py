"""ussd menu and ussd session

Revision ID: f289e8510444
Revises: 
Create Date: 2020-07-14 21:37:13.014200

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql


# revision identifiers, used by Alembic.
revision = 'f289e8510444'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('ussd_menus',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('created', sa.DateTime(), nullable=True),
    sa.Column('updated', sa.DateTime(), nullable=True),
    sa.Column('name', sa.String(), nullable=False),
    sa.Column('description', sa.String(), nullable=True),
    sa.Column('parent_id', sa.Integer(), nullable=True),
    sa.Column('display_text_en', sa.String(), nullable=False),
    sa.Column('display_text_sw', sa.String(), nullable=False),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_ussd_menus_name'), 'ussd_menus', ['name'], unique=True)
    op.create_table('ussd_sessions',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('authorising_user_id', sa.Integer(), nullable=True),
    sa.Column('created', sa.DateTime(), nullable=True),
    sa.Column('updated', sa.DateTime(), nullable=True),
    sa.Column('session_id', sa.String(), nullable=False),
    sa.Column('service_code', sa.String(), nullable=False),
    sa.Column('msisdn', sa.String(), nullable=False),
    sa.Column('user_input', sa.String(), nullable=True),
    sa.Column('ussd_menu_id', sa.Integer(), nullable=False),
    sa.Column('state', sa.String(), nullable=False),
    sa.Column('sessions_data', postgresql.JSON(astext_type=sa.Text()), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_ussd_sessions_session_id'), 'ussd_sessions', ['session_id'], unique=True)


def downgrade():
    op.drop_index(op.f('ix_ussd_sessions_session_id'), table_name='ussd_sessions')
    op.drop_table('ussd_sessions')
    op.drop_index(op.f('ix_ussd_menus_name'), table_name='ussd_menus')
    op.drop_table('ussd_menus')
