"""rename and tweak ussd tables

Revision ID: fc38e0d96520
Revises: f289e8510444
Create Date: 2020-07-14 21:41:28.687661

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql


# revision identifiers, used by Alembic.
revision = 'fc38e0d96520'
down_revision = 'f289e8510444'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('ussd_menu',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('created', sa.DateTime(), nullable=True),
    sa.Column('updated', sa.DateTime(), nullable=True),
    sa.Column('name', sa.String(), nullable=False),
    sa.Column('description', sa.String(), nullable=True),
    sa.Column('parent_id', sa.Integer(), nullable=True),
    sa.Column('display_key', sa.String(), nullable=False),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_ussd_menu_name'), 'ussd_menu', ['name'], unique=True)
    op.create_table('ussd_session',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('created', sa.DateTime(), nullable=True),
    sa.Column('updated', sa.DateTime(), nullable=True),
    sa.Column('session_id', sa.String(), nullable=False),
    sa.Column('service_code', sa.String(), nullable=False),
    sa.Column('msisdn', sa.String(), nullable=False),
    sa.Column('user_input', sa.String(), nullable=True),
    sa.Column('state', sa.String(), nullable=False),
    sa.Column('session_data', postgresql.JSON(astext_type=sa.Text()), nullable=True),
    sa.Column('ussd_menu_id', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['ussd_menu_id'], ['ussd_menu.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_ussd_session_session_id'), 'ussd_session', ['session_id'], unique=True)
    op.drop_index('ix_ussd_sessions_session_id', table_name='ussd_sessions')
    op.drop_table('ussd_sessions')
    op.drop_index('ix_ussd_menus_name', table_name='ussd_menus')
    op.drop_table('ussd_menus')


def downgrade():
    op.create_table('ussd_menus',
    sa.Column('id', sa.INTEGER(), autoincrement=True, nullable=False),
    sa.Column('created', postgresql.TIMESTAMP(), autoincrement=False, nullable=True),
    sa.Column('updated', postgresql.TIMESTAMP(), autoincrement=False, nullable=True),
    sa.Column('name', sa.VARCHAR(), autoincrement=False, nullable=False),
    sa.Column('description', sa.VARCHAR(), autoincrement=False, nullable=True),
    sa.Column('parent_id', sa.INTEGER(), autoincrement=False, nullable=True),
    sa.Column('display_key', sa.VARCHAR(), autoincrement=False, nullable=False),
    sa.PrimaryKeyConstraint('id', name='ussd_menus_pkey')
    )
    op.create_index('ix_ussd_menus_name', 'ussd_menus', ['name'], unique=True)
    op.create_table('ussd_sessions',
    sa.Column('id', sa.INTEGER(), autoincrement=True, nullable=False),
    sa.Column('created', postgresql.TIMESTAMP(), autoincrement=False, nullable=True),
    sa.Column('updated', postgresql.TIMESTAMP(), autoincrement=False, nullable=True),
    sa.Column('session_id', sa.VARCHAR(), autoincrement=False, nullable=False),
    sa.Column('service_code', sa.VARCHAR(), autoincrement=False, nullable=False),
    sa.Column('msisdn', sa.VARCHAR(), autoincrement=False, nullable=False),
    sa.Column('user_input', sa.VARCHAR(), autoincrement=False, nullable=True),
    sa.Column('ussd_menu_id', sa.INTEGER(), autoincrement=False, nullable=False),
    sa.Column('state', sa.VARCHAR(), autoincrement=False, nullable=False),
    sa.Column('session_data', postgresql.JSON(astext_type=sa.Text()), autoincrement=False, nullable=True),
    sa.PrimaryKeyConstraint('id', name='ussd_sessions_pkey')
    )
    op.create_index('ix_ussd_sessions_session_id', 'ussd_sessions', ['session_id'], unique=True)
    op.drop_index(op.f('ix_ussd_session_session_id'), table_name='ussd_session')
    op.drop_table('ussd_session')
    op.drop_index(op.f('ix_ussd_menu_name'), table_name='ussd_menu')
    op.drop_table('ussd_menu')
