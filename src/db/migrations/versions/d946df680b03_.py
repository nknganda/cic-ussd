"""remove notification

Revision ID: d946df680b03
Revises: c2bda0add384
Create Date: 2020-10-13 20:58:19.108835

"""
from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = 'd946df680b03'
down_revision = 'c2bda0add384'
branch_labels = None
depends_on = None

status_enum = sa.Enum(
    'UNKNOWN',  # the state of the message is not known
    name='notification_status',
)

transport_enum = sa.Enum(
    'SMS',
    name='notification_transport',
)


def upgrade():
    op.drop_index('notification_recipient_transport_idx')
    op.drop_table('notification')
    status_enum.drop(op.get_bind(), checkfirst=False)
    transport_enum.drop(op.get_bind(), checkfirst=False)


def downgrade():
    op.create_table('notification',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('transport', transport_enum, nullable=False),
                    sa.Column('status', status_enum, nullable=False),
                    sa.Column('status_code', sa.String(), nullable=True),
                    sa.Column('status_serial', sa.Integer(), nullable=False, server_default='0'),
                    sa.Column('recipient', sa.String(), nullable=False),
                    sa.Column('created', sa.DateTime(), nullable=False),
                    sa.Column('updated', sa.DateTime(), nullable=False),
                    sa.Column('content', sa.String(), nullable=False),
                    sa.PrimaryKeyConstraint('id'),
                    )
    op.create_index('notification_recipient_transport_idx', 'notification', ['transport', 'recipient'], schema=None,
                    unique=False)
