class VersionTooLowError(Exception):
    """Raised when the session version doesn't match latest version."""
    pass


class SessionNotFoundError(Exception):
    """Raised when queried session is not found in memory."""
    pass
