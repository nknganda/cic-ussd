# standard import
import os
import logging
import urllib
import json

# third-party imports
# this must be included for the package to be recognized as a tasks package
import celery
from celery import Celery

# export external celery task modules
from tasks.user import register_platform_user
from tasks.foo import log_it_plz
from tasks.ussd import persist_session_to_db
