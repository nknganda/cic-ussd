# standard imports
import logging
import json
from datetime import timedelta

# third party imports
import celery

# local imports
from models.ussd import UssdSession as US
from models.user import User
from session.ussd_session import UssdSession
from util import create_session
from error import SessionNotFoundError

celery_app = celery.current_app
logg = logging.getLogger()


class SqlAlchemyTask(celery.Task):
    """
    An abstract Celery Task that ensures that the connection to the database is closed on task completion.
    """
    abstract = True
    Session = create_session()

    def after_return(self, status, retval, task_id, args, kwargs, einfo):
        self.Session.close()

    def on_success(self, ret, task_id, args, kwargs):
        logg.debug('>> cb ok {} {} {} {}'.format(ret, task_id, args, kwargs))

    def on_failure(self, exc, task_id, args, kwargs, einfo):
        logg.debug('>> cb fail {} {} {} {} {}'.format(exc, task_id, args, kwargs, einfo))
        super().on_failure(exc, task_id, args, kwargs, einfo)


@celery_app.task(base=SqlAlchemyTask)
def persist_session_to_db(session_id: str) -> str:
    """
    This task initiates the saving of the session object to the database and it's removal from the in-memory storage.
    :param session_id: The session id of the session to be saved.
    :type session_id: str.
    :return: The representation of the newly created database object or en error message if session is not found.
    :rtype: str.
    :raises SessionNotFoundError: If the session object is not found in memory.
    :raises VersionTooLowError: If the session's version doesn't match the latest version.
    """
    session = UssdSession.r.get(session_id)
    if session:
        session = json.loads(session)
        db_session = US.session.query(US).filter_by(session_id=session_id).first()
        if db_session:
            db_session.update(
                user_input=session.get('user_input'),
                state=session.get('state'),
                ussd_menu=session.get('ussd_menu'),
                version=session.get('version')
            )
        else:
            user = User.session.query(User).filter_by(phone_number=session.get('msisdn')).first()
            db_session = US(
                session_id=session_id,
                service_code=session.get('service_code'),
                msisdn=session.get('msisdn'),
                user_input=session.get('user_input'),
                state=session.get('state'),
                session_data=session.get('session_data'),
                ussd_menu=session.get('ussd_menu'),
                version=session.get('version'),
                user_id=user.id
            )
        US.session.add(db_session)
        US.session.commit()
        UssdSession.r.expire(session_id, timedelta(minutes=1))
        return str(db_session)
    else:
        raise SessionNotFoundError('Session does not exist!')
