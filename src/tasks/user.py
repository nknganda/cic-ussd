# standard imports
import json
from urllib import request
import logging
import uuid

# third-party imports
import celery

# local imports
from transport.httpjson import HTTPJSONTransport

platform_transport = HTTPJSONTransport()

celery_app = celery.current_app
logg = logging.getLogger()


class UserTask(celery.Task):

    def on_success(self, ret, task, args, kwargs):
        logg.debug('>> cb ok {} {} {} {}'.format(ret, task, args, kwargs))

    def on_failure(self, exc, task, args, kwargs, einfo):
        logg.debug('>> cb fail {} {} {} {} {}'.format(exc, task, args, kwargs, einfo))
        super().on_failure(exc, task, args, kwargs, einfo)


# must have an http tranport
@celery_app.task(base=UserTask, bind=True)
def register_platform_user(self, phone, *args, **kwargs):
    endpoint = 'http://{}:{}/{}'.format(kwargs.get('host'), kwargs.get('port'), kwargs.get('endpoint'))
    logg.debug('endpoint {}'.format(endpoint))
    r = platform_transport.get(endpoint, headers={'X-FOO':'bar'}, data={'phone':phone})
    logg.debug('registergot {}'.format(r))
    return r
