# standard imports
import logging

# third-party imports
from sqlalchemy import Column, String, Integer, ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import JSON
from sqlalchemy.orm.attributes import flag_modified

# local imports
from models.base import SessionBase
from error import VersionTooLowError

logg = logging.getLogger(__name__)


class UssdSession(SessionBase):
    __tablename__ = 'ussd_session'

    session_id = Column(String, nullable=False, index=True, unique=True)
    service_code = Column(String, nullable=False)
    msisdn = Column(String, nullable=False)
    user_input = Column(String)
    state = Column(String, nullable=False)
    session_data = Column(JSON)
    version = Column(Integer, nullable=False)

    ussd_menu = Column(JSON, nullable=False)
    user_id = Column(Integer, ForeignKey('user.id'), nullable=False)
    user = relationship('User', back_populates='sessions')

    def set_data(self, key, value):
        if self.session_data is None:
            self.session_data = {}
        self.session_data[key] = value

        # https://stackoverflow.com/questions/42559434/updates-to-json-field-dont-persist-to-db
        flag_modified(self, "session_data")
        self.session.add(self)

    def get_data(self, key):
        if self.session_data is not None:
            return self.session_data.get(key)
        else:
            return None

    def check_version(self, new_version):
        if new_version <= self.version:
            raise VersionTooLowError('New session version number is not greater than last saved version!')

    def update(self, user_input, state, ussd_menu, version):
        self.check_version(version)
        self.user_input = user_input
        self.state = state
        self.ussd_menu = ussd_menu
        self.version = version
        self.session.add(self)

    @staticmethod
    def have_session_for_phone(phone):
        r = UssdSession.session.query(UssdSession).filter_by(msisdn=phone).first()
        return r != None
