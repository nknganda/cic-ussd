# third party imports
from sqlalchemy import Column, String
from sqlalchemy.orm import relationship

# local imports
from models.base import SessionBase
from util import check_password_hash, create_password_hash


class User(SessionBase):
    """
    This class defines a user record. It outlines three attributes for each record:
     - blockchain address
     - password hash
     - phone_number
     along with these attributes, it defines functions responsible for hashing the user's corresponding password and
     subsequently verifying a password's validity given an input to compare against the persisted hash.
    """
    __tablename__ = 'user'

    blockchain_address = Column(String)
    phone_number = Column(String)
    password_hash = Column(String)

    sessions = relationship('UssdSession', back_populates='user', lazy='dynamic')

    def __repr__(self):
        return f'<User: {self.blockchain_address}>'

    def create_password(self, password):
        """This method takes a password value and hashes the value before assigning it to the corresponding
        `hashed_password` attribute in the user record.
        :param password: A password value
        :type password: str
        """
        self.password_hash = create_password_hash(password)

    def verify_password(self, password):
        """This method takes a password value and compares it to the user's corresponding `hashed_password` value to
        establish password validity.
        :param password: A password value
        :type password: str
        :return: Pin validity
        :rtype: boolean
        """
        return check_password_hash(password, self.password_hash)
