# standard imports
import datetime

# third-party imports
from sqlalchemy import Column, Integer, DateTime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker


Model = declarative_base(name='Model')

class SessionBase(Model):
    __abstract__ = True
 
    id = Column(Integer, primary_key=True)
    created = Column(DateTime, default=datetime.datetime.utcnow)
    updated = Column(DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)

    engine = None
    session = None
    query = None


    @staticmethod
    def create_session(engine):
        SessionBase.set_engine(engine)
        return SessionBase.session


    @staticmethod
    def set_engine(engine):
        SessionBase.engine = engine
        session = sessionmaker(bind=engine)
        SessionBase.session = session()
        Model.metadata.create_all(bind=engine)
