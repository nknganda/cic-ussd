# standard imports
import logging
from typing import Optional

# third party imports
from tinydb import TinyDB, Query
from tinydb.table import Document

# local imports
from util import menu_file

logg = logging.getLogger()
db = TinyDB(menu_file, sort_keys=True, indent=4, separators=(',', ': '))
db = db.table('ussd_menu', cache_size=30)


class UssdMenu:
    """
    This class defines the USSD menu object that is called whenever a user makes transitions in the menu.
    """
    db = db
    Menu = Query()

    def __init__(self, name: str, description: str, parent: str, country: Optional[str] = 'Kenya',
                 gateway: Optional[str] = 'USSD') -> None:
        """
        This function is called whenever a USSD menu object is created and saves the instance to a JSON DB.
        :param name: The name of the menu and is used as it's unique identifier.
        :type name: str.
        :param description: A brief explanation of what the menu does.
        :type description: str.
        :param parent: The menu from which the current menu is called. Transitions move from parent to child menus.
        :type parent: str.
        :param country: The country from which the menu is created for and being used. Defaults to Kenya.
        :type country: str
        :param gateway: The gateway through which the menu is used. Defaults to USSD.
        :type gateway: str.
        :raises ValueError: If menu already exists.
        """
        self.name = name
        self.description = description
        self.parent = parent
        self.display_key = f'{gateway.lower()}.{country.lower()}.{name}'

        menu = UssdMenu.db.search(UssdMenu.Menu.name == name)
        if menu:
            raise ValueError('Menu already exists!')
        UssdMenu.db.insert({
            'name': self.name,
            'description': self.description,
            'parent': self.parent,
            'display_key': self.display_key
        })

    @staticmethod
    def find_by_name(name: str) -> Document:
        """
        This function attempts to fetch a menu from the JSON DB using the unique name.
        :param name: The name of the menu that is being searched for.
        :type name: str.
        :return: The function returns the queried menu in JSON format if found,
        else it returns the menu item for invalid requests.
        :rtype: Document.
        """
        menu = UssdMenu.db.search(UssdMenu.Menu.name == name)
        if not menu:
            logg.error("No USSD Menu with name {}".format(name))
            return UssdMenu.db.search(UssdMenu.Menu.name == 'exit_invalid_request')[0]
        else:
            return menu[0]

    @staticmethod
    def set_description(name: str, description: str) -> None:
        """
        This function updates the description for a specific menu in the JSON DB.
        :param name: The name of the menu whose description should be updated.
        :type name: str.
        :param description: The new menu description. On success it should overwrite the menu's previous description.
        :type description: str.
        """
        menu = UssdMenu.find_by_name(name)
        UssdMenu.db.update({'description': description}, UssdMenu.Menu.name == menu['name'])

    def parent_menu(self) -> Document:
        """
        This function fetches the parent menu of the menu instance it has been called on.
        :return: This function returns the menu's parent menu in JSON format.
        :rtype: Document.
        """
        return UssdMenu.find_by_name(self.parent)

    def __repr__(self) -> str:
        """
        This method return the object representation of the menu.
        :return: This function returns a string containing the object representation of the menu.
        :rtype: str.
        """
        return f"<UssdMenu {self.name} - {self.description}>"
