# standard imports
import logging

# third party imports
from transitions import Machine, State

# local imports
from models.ussd import UssdSession

logg = logging.getLogger(__name__)


class UssdStateMachine(Machine):
    """
    This class describes a finite state machine responsible for maintaining all the states that describe the ussd menu
    as well as providing a means for navigating through these states based on different user inputs.
    It defines different helper functions that co-ordinate with the stakeholder components of the ussd menu: i.e  the
    User, UssdSession, UssdMenu to facilitate user interaction with ussd menu.

    :param session: A Ussd session object that contains contextual data that informs the state machine's state changes.
    :type session: UssdSession
    :param user: A user interacting with the ussd menu.
    :type user: User
    """
    def __repr__(self):
        return f'<KenyaUssdStateMachine: {self.state}>'

    states = [
        State(name='complete', on_enter=['send_terms_to_user_if_required'])
    ]

    def __init__(self, session: UssdSession, user):
        self.session = session
        self.user = user
        super(UssdStateMachine, self).__init__(self, model=self, states=self.states, initial=session.state)
    logg.debug('The state machine interacts with the user object.')
