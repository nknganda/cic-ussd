# standard imports
import logging
from typing import Optional

# local imports
from models.ussd import UssdMenu, UssdSession
from processor.ussd_state_machine import UssdStateMachine

logg = logging.getLogger(__name__)


class UssdProcessor:
    """This class defines helper functions responsible for the processing and provision of appropriate responses to
    requests to access the USSD menu.
    """
    @staticmethod
    def process_request(session_id: str, user_input: str):
        """This function assesses a request based on the user from the request comes, the session_id and the user's input.
        It determines whether the request translates to a return to an existing session by checking whether the provided
        session id exists in the database or whether the creation of a new ussd session object is warranted.
        It then returns the appropriate ussd menu text values.

        :param session_id: A unique string value received from the AfricasTalking API to identify a ussd session.
        :type session_id: str
        :param user_input: The value a user enters in the ussd menu.
        :type user_input: str
        :return: A ussd menu's corresponding text value.
        :rtype: str
        """
        logg.debug('The process request function interacts with a user object.')

    @staticmethod
    def next_state(session: UssdSession, user_input: str):
        """This function navigates the state machine based on the ussd session object and user inputs it receives.
        It checks the user input and provides the successive state in the state machine. It then updates the session's
        state attribute with the new state.
        :param session: The Ussd session object necessary to determine the state machine's current and eventually
        subsequent state.
        :type session: UssdSession
        :param user_input: The value a user enters in the ussd menu.
        :type user_input: str
        :return: A string value corresponding the successive give a specific state in the state machine.
        """
        logg.debug('The next state function interacts with a user object.')

    @staticmethod
    def custom_display_text(menu: UssdMenu, ussd_session: UssdSession) -> str:
        """This function extracts the appropriate session data based on the current menu name. It then inserts them as
        keywords in the i18n function.
        :param menu: The USSD menu whose corresponding text values need contextual ussd session data.
        :type menu: UssdMenu
        :param ussd_session: The USSD session determining what user data needs to be extracted and added to the menu's
        text values.
        :type ussd_session: UssdSession
        :return: A string value corresponding the ussd menu's text value.
        """
        logg.debug('The custom display text function interacts with a user object.')
