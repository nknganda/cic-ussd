# standard imports
from random import randint

# third party imports
import pytest

# local imports
from menu.ussd_menu import UssdMenu
from session.ussd_session import UssdSession
from models.ussd import UssdSession as US
from models.user import User
from tasks.ussd import persist_session_to_db
from helpers.utils import clear_test_data
from error import SessionNotFoundError


def test_persist_session_to_db_task(
        init_database,
        load_config,
        celery_worker,
):
    user = User(
        blockchain_address='0x0ebdea8612c1b05d952c036859266c7f2cfcd6a29842d9c6cce3b9f1ba427588',
        phone_number='+25498765432',
        password_hash='00000000'
    )
    session = User.session
    session.add(user)
    session.commit()
    UssdMenu(name='init', description='Buzz', parent=None)
    UssdMenu(name='go', description='Start', parent='init')
    session_id = f'AT{randint(000000, 999999)}'
    UssdSession(session_id=session_id, service_code='*123#', msisdn='+25498765432', user_input='5', state='init')
    r = persist_session_to_db.delay(session_id)
    r.get()
    UssdSession(session_id=session_id, service_code='*123#', msisdn='+25498765432', user_input='2', state='go')
    r = persist_session_to_db.delay(session_id)
    r.get()
    db_session = US.session.query(US).filter_by(session_id=session_id).first()
    assert db_session.version == 2
    assert db_session.state == 'go'
    assert US.have_session_for_phone('+25498765432') is True
    files = [load_config.get('USSD_MENU')]
    clear_test_data(files)


def test_session_not_found_error(
        init_database,
        celery_worker
):
    with pytest.raises(SessionNotFoundError) as e:
        r = persist_session_to_db.delay('AT12345678')
        assert r.get()
    assert str(e.value) == 'Session does not exist!'
