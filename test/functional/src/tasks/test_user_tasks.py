# standard import
import os
import time

# third party imports
import pytest
import celery
from celery.utils.log import get_logger

# local imports
from tasks.user import register_platform_user
from adapters.user import UserAdapter

# platform imports
from eth_manager.celery_tasks import create_new_blockchain_wallet

logg = get_logger(__name__)


def test_task_register_platform_user(
        load_config,
        celery_worker,
        http_server,
):
    d = register_platform_user('+253112415',
                               host=http_server.server_address[0],
                               port=http_server.server_address[1],
                               endpoint=os.path.join(load_config.get('API_PREFIX'), 'register', ''),
                               )
    assert d == 201


# Hmm, this just falls through
def test_register_without_key(
        load_config,
        http_server,
        celery_app,
        celery_worker,
):
    load_config.store['PLATFORM_HOST'] = http_server.server_address[0]
    load_config.store['PLATFORM_PORT'] = str(http_server.server_address[1])
    ua = UserAdapter(load_config, celery_app, '+25412121212')
    r = ua.register_without_key()
    r.get()
    assert r.id is not None
