def clear_test_data(files):
    for file in files:
        with open(file, 'r+') as f:
            f.truncate(0)