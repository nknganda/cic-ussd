# local imports
from menu.ussd_menu import UssdMenu
from helpers.utils import clear_test_data


def test_ussd_menu(load_config):
    UssdMenu(name='foo', description='foo-bar', parent=None)
    assert UssdMenu.find_by_name('foo')['description'] == 'foo-bar'
    UssdMenu.set_description('foo', 'bar')
    assert UssdMenu.find_by_name('foo')['description'] == 'bar'
    menu2 = UssdMenu(name='fizz', description='buzz', parent='foo')
    assert menu2.parent_menu()['description'] == 'bar'
    file = [load_config.get('USSD_MENU')]
    clear_test_data(file)
