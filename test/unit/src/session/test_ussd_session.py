# local imports
from menu.ussd_menu import UssdMenu
from session.ussd_session import UssdSession
from helpers.utils import clear_test_data


def test_ussd_session(load_config):
    UssdMenu(name='test', description='Test Session', parent=None)
    session = UssdSession(session_id='AT4873B5', service_code='*123#', msisdn='123', user_input='5', state='test')
    assert UssdMenu.find_by_name('test') == session.ussd_menu
    assert session.get_data('foo') is None
    session.set_data('foo', 'bar')
    assert session.get_data('foo') == 'bar'
    session.set_data('fizz', 'buzz')
    assert session.get_data('fizz') == 'buzz'
    assert session.get_data('foo') == 'bar'
    files = [load_config.get('USSD_MENU')]
    clear_test_data(files)
