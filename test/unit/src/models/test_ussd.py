# third party imports
import pytest

# local imports
from models.user import User
from models.ussd import UssdSession
from error import VersionTooLowError


def test_ussd_session(init_database):
    user = User(
        blockchain_address='0x0ebdea8612c1b05d952c036859266c7f2cfcd6a29842d9c6cce3b9f1ba427588',
        phone_number='+25412345678',
        password_hash='00000000'
    )
    Session = User.session
    Session.add(user)
    Session.commit()

    session = UssdSession(
        session_id='AT65423',
        service_code='*123#',
        msisdn='+25412345678',
        user_input='1',
        state='start',
        session_data={},
        ussd_menu={'name': 'start', 'description': 'First Menu'},
        version=1,
        user_id=user.id
    )
    session.set_data('foo', 'bar')
    assert session.get_data('foo') == 'bar'
    session.update(
        user_input='3',
        state='next',
        ussd_menu={'name': 'next', 'description': 'Second Menu'},
        version=2
    )
    assert session.version == 2
    Session.add(session)
    Session.commit()

    assert UssdSession.have_session_for_phone('+25412345678') is True


def test_version_too_low_error(init_database):
    with pytest.raises(VersionTooLowError) as e:
        user = User(
            blockchain_address='0x0ebdea8612c1b05d952c036859266c7f2cfcd6a29842d9c6cce3b9f1ba427588',
            phone_number='+25498765432',
            password_hash='00000000'
        )
        Session = User.session
        Session.add(user)
        Session.commit()

        session = UssdSession(
            session_id='AT38745',
            service_code='*123#',
            msisdn='+25498765432',
            user_input='1',
            state='start',
            session_data={},
            ussd_menu={'name': 'start', 'description': 'First Menu'},
            version=3,
            user_id=user.id
        )
        assert session.check_version(1)
        assert session.check_version(3)
    assert str(e.value) == 'New session version number is not greater than last saved version!'
