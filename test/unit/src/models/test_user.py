"""Tests the persistence of the user record and associated functions to the user object"""

# standard imports
import pytest

# platform imports
from models.user import User


def test_user(init_database):
    user = User(blockchain_address='0x0ebdea8612c1b05d952c036859266c7f2cfcd6a29842d9c6cce3b9f1ba427588',
                phone_number='+254712345678')
    user.create_password('0000')

    session = User.session
    session.add(user)
    session.commit()

    queried_user = session.query(User).get(1)
    assert (queried_user.blockchain_address == '0x0ebdea8612c1b05d952c036859266c7f2cfcd6a29842d9c6cce3b9f1ba427588')
    assert(queried_user.phone_number == '+254712345678')
    assert queried_user.verify_password('0000') is True
