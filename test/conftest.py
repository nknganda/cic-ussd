# standard imports
import os
import sys
import pytest
import logging
import threading
from http.server import BaseHTTPRequestHandler, HTTPServer

# third party imports
from confini import Config
from sqlalchemy import create_engine, MetaData
import alembic
from alembic.config import Config as AlembicConfig

# local imports
from models.base import SessionBase

logg = logging.getLogger()


class TestHandler(BaseHTTPRequestHandler):

    def do_POST(self, *args, **kwargs):
        l = 0
        logg.debug('http mock request {}'.format(self.requestline))
        for h in self.headers:
            logg.debug('http mock header {}: {}'.format(h, self.headers[h]))
            if h.lower() == 'content-length':
                l = int(self.headers[h])
        data = self.rfile.read(l)
        logg.debug('http mock read {}'.format(data))
        self.send_response(201, 'Created')
        self.end_headers()
        return True


@pytest.fixture(scope='function')
def http_server():
    srv = HTTPServer(('',0), TestHandler)
    logg.debug('mock http server on {}:{}'.format(srv.server_address[0], srv.server_address[1]))
    t = threading.Thread(None, srv.serve_forever, 'pytest_httpd')
    t.start()
    yield srv
    logg.debug(srv)
    srv.shutdown()
    t.join()


@pytest.fixture(scope='session')
def load_config():
    config = Config(f'{os.environ.get("CONFIG_DIR")}/{os.environ.get("DEPLOYMENT_NAME")}/')
    config.process(set_as_current=True)
    logg.debug('config loaded\n{}'.format(config))
    return config


@pytest.fixture(scope='function')
def init_database(
        load_config,
        ):

    dsn = 'postgresql+psycopg2://{}:{}@{}:{}/{}'.format(
        load_config.get('DATABASE_USER'),
        load_config.get('DATABASE_PASSWORD'),
        load_config.get('DATABASE_HOST'),
        load_config.get('DATABASE_PORT'),
        load_config.get('DATABASE_NAME'),    
    )

    rootdir = os.path.dirname(os.path.dirname(__file__))
    migrationsdir = os.path.join(rootdir, 'src', 'db', 'migrations')
    logg.info('using migrationsdir {}'.format(migrationsdir))

    ac = AlembicConfig(os.path.join(migrationsdir, 'alembic.ini'))
    ac.set_main_option('script_location', migrationsdir)
    ac.set_main_option('sqlalchemy.url', dsn)

    alembic.command.downgrade(ac, 'base')
    alembic.command.upgrade(ac, 'head')

    db_engine = create_engine(dsn)

    session = SessionBase.create_session(db_engine)
    yield session

    session.commit()
    session.close()


@pytest.fixture(scope='session')
def alembic_config():
    root_directory = os.path.dirname(os.path.dirname(__file__))
    migrations_directory = os.path.join(root_directory, 'src', 'db', 'migrations')
    file = os.path.join(migrations_directory, 'alembic.ini')
    return {
        'file': file,
        'script_location': migrations_directory
    }


@pytest.fixture(scope='session')
def alembic_engine(load_config):
    database_uri = 'postgresql+psycopg2://{}:{}@{}:{}/{}'.format(
        load_config.get('DATABASE_USER'),
        load_config.get('DATABASE_PASSWORD'),
        load_config.get('DATABASE_HOST'),
        load_config.get('DATABASE_PORT'),
        load_config.get('DATABASE_NAME'),
    )
    database_engine = create_engine(database_uri)
    return database_engine


# celery fixtures

@pytest.fixture(scope='session')
def celery_includes():
    return [
        'tasks.user',
        'tasks.ussd',
        'eth_manager.celery_tasks',
    ]


@pytest.fixture(scope='session')
def celery_config():
    return {
            'broker_url': 'redis://localhost:6379',
            'result_backend': 'redis://localhost:6379',
            }

@pytest.fixture(scope='session')
def celery_worker_parameters():
    return {
    #        'queues': ('ussdee'),
            }

@pytest.fixture(scope='session')
def celery_enable_logging():
    return True
